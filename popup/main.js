function transformURL() {
  let ytnc = "https://www.youtube-nocookie.com/embed/";
  let url = document.getElementById('url_input').value;
  let id = null;

  let re_watch = /.*youtube.com\/watch\?v=.*/;
  let re_share = /.*youtu.be\/.*/;
  let re_ytnc = /.*youtube-nocookie.com\/embed\/.*/;

  if (url.match(re_ytnc)) return url;

  if (url.match(re_watch)) id = url.split("?v=", 2)[1];
  else if (url.match(re_share)) id = url.split(".be/")[1];

  if (id != null) {
    if (id.includes("&")) id = id.split("&")[0];
    return ytnc + id;
  }
  return null;
}

function openURL() {
  let url = transformURL();

  if (url != null) window.open(url, '_blank').focus();
}

function copyURL() {
  let elem = document.getElementById('url_input');
  elem.value = transformURL();
  elem.select();
  document.execCommand("copy");
}

document.getElementById("copy").addEventListener("click", copyURL);
document.getElementById("open").addEventListener("click", openURL);
