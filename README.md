# youtube-nocookie

**youtube-nocookie** is a Firefox add-on that allows you to change youtube's URL into youtube-nocookie's URL for better privacy.

![Preview](screenshots/preview.png)

## Getting started

You can download the add-on from the mozilla's website, [here](https://addons.mozilla.org/en-US/firefox/addon/youtube-privacy/).

## Built with

- HTML / CSS
- JS

## License

This project is licensed under the MIT License.